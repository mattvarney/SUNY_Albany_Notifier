<?php

$url = 'http://www.albany.edu/cgi-bin/general-search/search.pl';

$Course_Subject = $_POST['Course_Subject'];
$Course_Number = $_POST['Course_Number'];
$Department_or_Program = $_POST['Department_or_Program'];
$fields = array(
  'Course_Subject' => urlencode($Course_Subject),
  'Course_Number' => urlencode($Course_Number),
  'Department_or_Program' => urlencode($Department_or_Program)
);

foreach($fields as $key => $value) {
  $fields_string .= $key.'='.$value.'&';
}
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);

$pattern = '/Seats remaining as of last update: <B>([0-9]?)</B><BR>/';
preg_match($pattern, $result, $matches);
echo $matches[1];

?>
